[<AutoOpen>]
module Expecto

open Expecto
open System.Runtime.CompilerServices

type File =
  static member Name () =
    let st = System.Diagnostics.StackTrace (0, true)
    let name = st.GetFrames() |> Seq.map ( fun x -> x.GetMethod().DeclaringType.Name ) |> Seq.filter ( fun x -> x.[0] = '$' ) |> Seq.head
    name.Replace("$fsx", "").Replace("$", "")


let (==) actual expected expectation =
  testCase expectation <| fun () -> Expect.equal actual expected ""

let (!=) actual expected expectation =
  testCase expectation <| fun () -> Expect.notEqual actual expected ""

type Tests = TestsAttribute

type FTests = FTestsAttribute

type PTests = PTestsAttribute

let testCase fn = fn() |> testCase

let test = Expecto.Tests.testCase

let testList tests = testList (File.Name()) tests



let shouldEqual expected actual =
  Expect.equal actual expected ""

let shouldBe expected actual =
  Expect.equal actual expected ""

let shouldBeEmpty x =
  let length = x |> List.length
  Expect.equal length 0 ( sprintf "List was not empty - it contains: %A" x )

let shouldBeNull x =
  Expect.isNull x ( sprintf "Expected null but was %O" x )


let shouldContain item sequence =
    Expect.contains sequence item ""
