open System
open Serilog
open Serilog.Sinks.Elasticsearch
open Microsoft.FSharp.Core.Printf

let mutable private log = Unchecked.defaultof<Core.Logger>

let private createLog (minimumLevel: Configuration.LoggerMinimumLevelConfiguration -> unit -> LoggerConfiguration) =
  let loggerConfig = LoggerConfiguration().MinimumLevel |> minimumLevel
  log <- loggerConfig().WriteTo.ColoredConsole().CreateLogger()

createLog (fun x -> x.Debug)

type MinimumLevel =
  | Debug
  | Info
  | Error
  | Fatal
  | Warn

let setMinimumLevel =
  function
  | Info -> createLog (fun x -> x.Information)
  | Debug -> createLog (fun x -> x.Debug)
  | Error -> createLog (fun x -> x.Error)
  | Fatal -> createLog (fun x -> x.Fatal)
  | Warn -> createLog (fun x -> x.Warning)

let useElasticSearch useDebug elasticUrl =
  let lc = LoggerConfiguration().MinimumLevel
  let lc =
    match useDebug with
    | true -> lc.Debug()
    |_ -> lc.Information()
  let lc = Uri elasticUrl |> ElasticsearchSinkOptions |> lc.WriteTo.Elasticsearch //"http://localhost:9200"
  log <- lc.CreateLogger()

type private Level = Debug | Info | Warn | Error | Fatal

let private logf level =
  match level with
  | Debug -> log.Debug
  | Info -> log.Information
  | Warn -> log.Warning
  | Error -> log.Error
  | Fatal -> log.Fatal
  |> ksprintf

let debugf fmt = logf Debug fmt
let infof fmt = logf Info fmt
let warnf fmt = logf Warn fmt
let errorf fmt = logf Error fmt
let fatalf fmt = logf Fatal fmt

let debug o = debugf "%O" o
let info o = infof "%O" o
let warn o = warnf "%O" o
let error o = errorf "%O" o
let fatal o = fatalf "%O" o

let raise ex =
  fatal ex
  raise ex