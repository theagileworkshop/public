open Fake
open Fake.Git





let gitOn repo cmd =
    Printf.ksprintf (fun x -> directRunGitCommandAndFail repo x) cmd



let prepareFunctionTo environment functionName =
    printfn "Preparing %s" functionName
    let destinationFolder = ( sprintf "deploy/%s" functionName )
    let sourceFolder = ( sprintf "build/%s" functionName )

    CreateDir destinationFolder
    CopyFiles destinationFolder ( SetBaseDir sourceFolder
        !! "/*.dll"
        -- "FSharp.Core.*"
        ++ "function.json"
        )

    CopyFile ( sprintf "%s/config.yml" destinationFolder ) ( sprintf "%s/config.%s.yml" sourceFolder environment  )



let deployTo environment kuduGitUrl (functions:string list) =
    let git cmd = gitOn "deploy" cmd

    DeleteDir "deploy"
    clone "." kuduGitUrl "deploy"
    git "rm *"

    let deployFunction fn = prepareFunctionTo environment fn

    Seq.iter deployFunction functions

    git "add -A"
    git "commit --amend -m \"Deployed on %s\"" ( System.DateTime.Now.ToString( "dd-MM-yyyy - HH:mm:ss" ) )
    let success,result,response = runGitCommand "deploy" "push --force"

    printfn "GIT response: %s" response
    Seq.iter (fun x -> printfn "GIT : %s" x ) result

    if response.ToLower().Contains( "error" ) then
        failwith response


